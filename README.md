<p align="center">
  <img src="img/tensorsem.png" width="300px"></img>
  <br/>
  <span>
    <a href="https://CRAN.R-project.org/package=tensorsem"><img src="http://www.r-pkg.org/badges/version/tensorsem"></img></a>
    <a href="https://travis-ci.org/vankesteren/tensorsem"><img src="https://travis-ci.com/vankesteren/tensorsem.svg?token=sWy8hKyU5pssaiyciVfB&branch=master"></img></a>
  </span>
  <h5 align="center">Structural Equation Modeling using TensorFlow</h5>
</p>
<br/>

## Description
An `R` package for structural equation modeling using TensorFlow

## Installation
1. Install tensorflow for R by following the instructions on the [RStudio website](https://tensorflow.rstudio.com/tensorflow/)
2. Install the tensorsem package from this repository as follows:
```r
devtools::install_github("vankesteren/tensorsem")
```
